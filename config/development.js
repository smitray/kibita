module.exports = {
  webpack: {
    sourceMap: false
  },
  server: {
    port: 3000,
    compress: false
  },
  db: {
		host: 'localhost',
		dbName: 'kibita',
		debug: false,
		options: {
			userName: false,
			passWord: false,
			port: 27017
		}
	},
	session: {
		secret: 'yoursecretkey',
		db: {
			require: true,
			collection: 'session'
		},
		maxage: '3600000'
	}
}
