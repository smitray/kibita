var path = require('path');

module.exports = {
  paths: {
    app: {
      client: path.resolve(__dirname, '../client'),
      server: path.resolve(__dirname, '../server/app'),
    },
    dist: {
      path: path.resolve(__dirname, '../public'),
      publicPath: '/'
    },
    rootPath: path.resolve(__dirname, '../'),
    assetsPath: path.resolve(__dirname, '../public/assets'),
    core: path.resolve(__dirname, '../server/core')
  },
  favicon: 'favicon.ico',
  googleAnalyticID: 'UA-36546786-1',
  social: {
		facebook: {
			clientID: '1141626679233371',
			clientSecret: '048915a0b1574898809bb6514024f1b1',
      callback: '/auth/fb/fb_cb'
		},
    google: {
      clientID: '986336024103-nsu60vtp6ch74627s1n7qhb324j80h1d.apps.googleusercontent.com',
      clientSecret: 'pWSZvwx45wK86wFrxne3PQzW',
      callback: '/auth/google/g_cb'
    },
    twitter: {
      clientID: 'gV9eNUplPuqdwAmwqrXVCOItI',
      clientSecret: 'bb9uuh16uMCWxfV0II7bxq6w1Tw2FgDJkjsiSQ7zcl28TnmlE9',
      callback: '/auth/twitter/tw_cb'
    },
    github: {
      clientID: '81c4a35b7dc70bfd1d15',
      clientSecret: '18785524ec9b60a54e2ef164dd73373db1c982eb',
      callback: '/auth/github/git_cb'
    },
    paypal: {
      clientID: '',
      clientSecret: '',
      callback: ''
    }
	},
  webpack: {
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    bundleAnalyzerReport: false
  }
}
