module.exports = {
  webpack: {
    sourceMap: false,
    bundleAnalyzerReport: false
  },
  server: {
    port: 8000,
    compress: false
  },
  db: {
		host: 'localhost',
		dbName: 'ceapp',
		debug: false,
		options: {
			userName: false,
			passWord: false,
			port: 27017
		}
	},
	session: {
		secret: 'yoursecretkey',
		db: {
			require: true,
			collection: 'session'
		},
		maxage: '3600000'
	}
}
