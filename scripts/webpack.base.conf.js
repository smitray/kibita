import config from 'config';
import EsfrndFrmtr from 'eslint-friendly-formatter';
import { join } from 'path';
import vueLoaderConfig from './vue.conf';


export default {
  context: config.get('paths.app.client'),
  entry: {
    app: './index.js'
  },
  output: {
    path: config.get('paths.dist.path'),
    publicPath: config.get('paths.dist.publicPath'),
    filename: 'assets/js/[name].[hash:7].js'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      assets: join(config.get('paths.app.client'), 'assets'),
      modules: join(config.get('paths.app.client'), 'modules'),
      shared: join(config.get('paths.app.client'), 'shared')
    }
  },
  module: {
    rules: [{
      test: /\.(js|vue)$/,
      loader: 'eslint-loader',
      enforce: 'pre',
      include: [config.get('paths.app.client')],
      options: {
        formatter: EsfrndFrmtr
      }
    }, {
      test: /\.vue$/,
      loader: 'vue-loader',
      options: vueLoaderConfig
    }, {
      test: /\.js$/,
      loader: 'babel-loader',
      include: [config.get('paths.app.client')],
      options: {
        babelrc: false,
        presets: [
          ['env', { modules: false }],
          'stage-2'
        ],
        plugins: ['transform-runtime'],
        comments: false,
        env: {
          test: {
            presets: ['env', 'stage-2'],
            plugins: ['istanbul']
          }
        }
      }
    }, {
      test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000,
        name: 'assets/img/[name].[hash:7].[ext]'
      }
    }, {
      test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000,
        name: 'assets/fonts/[name].[hash:7].[ext]'
      }
    }]
  }
};
