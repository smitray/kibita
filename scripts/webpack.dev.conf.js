import webpack from 'webpack';
import merge from 'webpack-merge';
import HtmlWebpack from 'html-webpack-plugin';
import FriendlyErrorsPlugin from 'friendly-errors-webpack-plugin';
import baseWebpackConfig from './webpack.base.conf';

Object.keys(baseWebpackConfig.entry).forEach((name) => {
  baseWebpackConfig.entry[name] = ['../scripts/dev-client'].concat(baseWebpackConfig.entry[name]);
});

export default merge(baseWebpackConfig, {
  devtool: '#cheap-module-eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(false)
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpack({
      template: './template.html'
    }),
    new FriendlyErrorsPlugin()
  ]
});
