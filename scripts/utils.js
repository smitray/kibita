import ExtractTextPlugin from 'extract-text-webpack-plugin';

export function cssLoaders(options) {
  options = options || {}; // eslint-disable-line

  const cssLoader = {
    loader: 'css-loader',
    options: {
      minimize: process.env.NODE_ENV === 'production',
      sourceMap: options.sourceMap
    }
  };

  function generateLoaders(loader, loaderOptions) {
    const loaders = [cssLoader];
    if (loader) {
      loaders.push({
        loader: `${loader}-loader`,
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      });
    }

    if (options.extract) {
      return ExtractTextPlugin.extract({
        use: loaders,
        fallback: 'vue-style-loader'
      });
    }
    return ['vue-style-loader'].concat(loaders);
  }

  return {
    css: generateLoaders(),
    postcss: generateLoaders(),
    less: generateLoaders('less'),
    sass: generateLoaders('sass', { indentedSyntax: true }),
    scss: generateLoaders('sass'),
    stylus: generateLoaders('stylus'),
    styl: generateLoaders('stylus')
  };
}

export function styleLoaders(options) {
  const output = [];
  const loaders = cssLoaders(options);
  Object.keys(loaders).forEach((key) => {
    const loader = loaders[key];
    output.push({
      test: new RegExp(`\\.${key}$`),
      use: loader
    });
  });
  return output;
}
