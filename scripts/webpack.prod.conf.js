import { resolve } from 'path';
import webpack from 'webpack';
import merge from 'webpack-merge';
import config from 'config';
// import CopyWebpackPlugin from 'copy-webpack-plugin';
import CompressionWebpackPlugin from 'compression-webpack-plugin';
import HtmlWebpack from 'html-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import OptimizeCSSPlugin from 'optimize-css-assets-webpack-plugin';
import { styleLoaders } from './utils';
import baseWebpackConfig from './webpack.base.conf';

const webpackConfig = merge(baseWebpackConfig, {
  module: {
    rules: styleLoaders({
      sourceMap: config.get('webpack.sourceMap'),
      extract: true
    })
  },
  devtool: config.get('webpack.sourceMap') ? '#source-map' : false,
  output: {
    path: config.get('paths.dist.path'),
    publicPath: config.get('paths.dist.publicPath'),
    filename: 'assets/js/[name].[chunkhash].js',
    chunkFilename: 'assets/js/[id].[chunkhash].js'
  },
  plugins: [
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(true)
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      sourceMap: true
    }),
    new ExtractTextPlugin({
      filename: 'assets/css/[name].[contenthash].css'
    }),
    new OptimizeCSSPlugin({
      cssProcessorOptions: {
        safe: true
      }
    }),
    new HtmlWebpack({
      template: './template.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      },
      chunksSortMode: 'dependency'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: module => (
        module.resource &&
        /\.js$/.test(module.resource) &&
        module.resource.indexOf(resolve(__dirname, '../node_modules')) === 0
      )
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
      chunks: ['vendor']
    })
  ]
});

if (config.get('webpack.productionGzip')) {
  webpackConfig.plugins.push(new CompressionWebpackPlugin({
    asset: '[path].gz[query]',
    algorithm: 'gzip',
    test: new RegExp(`\\.(${config.get('webpack.productionGzipExtensions').join('|')})$`),
    threshold: 10240,
    minRatio: 0.8
  }));
}

if (config.get('webpack.bundleAnalyzerReport')) {
  webpackConfig.plugins.push(new BundleAnalyzerPlugin());
}

export default webpackConfig;
