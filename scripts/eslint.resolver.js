const config = require('config');
const path = require('path');

module.exports = {
  context: config.get('paths.app.client'),
  entry: {
    app: './index.js'
  },
  output: {
    path: config.get('paths.dist.path'),
    publicPath: config.get('paths.dist.publicPath'),
    filename: 'assets/js/[name].[hash:7].js'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      assets: path.join(config.get('paths.app.client'), 'assets'),
      modules: path.join(config.get('paths.app.client'), 'modules'),
      shared: path.join(config.get('paths.app.client'), 'shared')
    }
  }
};
