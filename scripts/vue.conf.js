import config from 'config';
import isDev from 'isdev';
import { cssLoaders } from './utils';

export default {
  loaders: cssLoaders({
    sourceMap: config.get('webpack.sourceMap'),
    extract: !isDev
  })
};
