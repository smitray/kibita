import Vue from 'vue';
import { ContainerComponent, router, store } from 'shared';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(ContainerComponent)
});
