import Landing from './landing.comp';

export default [{
  path: '/',
  name: 'Home',
  component: Landing
}];
