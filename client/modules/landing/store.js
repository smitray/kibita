export default {
  namespaced: true,
  state: {
    value: 10
  },
  getters: {
    doubleValue: state => state.value * 2
  },
  mutations: {
    increase: (state) => {
      state.value *= 4;
    }
  }
};
