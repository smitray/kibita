import { routes as landingRoute, store as landingStore } from './landing';

export const allRoutes = [
  ...landingRoute
];

export const allStores = {
  landingStore
};
