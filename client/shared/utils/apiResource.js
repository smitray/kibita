import axios from 'axios';

const baseUrl = '/';
let config = {};

export const Api = {
  get(path, auth = false) {
    if (auth) {
      config = {
        headers: {
          Authorization: 'Bearer'
        }
      };
    }
    const req = axios.get(`${baseUrl}${path}`, config);
    return req.then(result => result)
      .catch((err) => {
        throw err;
      });
  },

  post(path, body, auth = false) {
    if (auth) {
      config = {
        headers: {
          Authorization: 'Bearer'
        }
      };
    }
    const req = axios.post(`${baseUrl}${path}`, body, config);
    return req.then(result => result)
      .catch((err) => {
        throw err;
      });
  },

  put(path, body, auth = false) {
    if (auth) {
      config = {
        headers: {
          Authorization: 'Bearer'
        }
      };
    }
    const req = axios.put(`${baseUrl}${path}`, body, config);
    return req.then(result => result)
      .catch((err) => {
        throw err;
      });
  },

  patch(path, body, auth = false) {
    if (auth) {
      config = {
        headers: {
          Authorization: 'Bearer'
        }
      };
    }
    const req = axios.patch(`${baseUrl}${path}`, body, config);
    return req.then(result => result)
      .catch((err) => {
        throw err;
      });
  },

  delete(path, auth = false) {
    if (auth) {
      config = {
        headers: {
          Authorization: 'Bearer'
        }
      };
    }
    const req = axios.delete(`${baseUrl}${path}`, config);
    return req.then(result => result)
      .catch((err) => {
        throw err;
      });
  }
};

// localhost:3000/users - get all
// localhost:3000/users - post
// localhost:3000/users/{id} - get single user
// localhost:3000/users/{id} - put update single user all content
// localhost:3000/users/{id} - patch update single user single content
// localhost:3000/users/{id} - delete single user


export class RestApi {
  constructor(path) {
    this.path = path;
  }
  getAll(auth = false) {
    return Api.get(this.path, auth);
  }
  getSingle(param, auth = false) {
    return Api.get(`${this.path}/${param}`, auth);
  }
  createNew(body, auth = false) {
    return Api.post(this.path, body, auth);
  }
  updateAll(param, body, auth = false) {
    return Api.put(`${this.path}/${param}`, body, auth);
  }
  updateSingle(param, body, auth = false) {
    return Api.patch(`${this.path}/${param}`, body, auth);
  }
  delete(param, auth = false) {
    return Api.delete(`${this.path}/${param}`, auth);
  }
}
