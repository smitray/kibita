import Vue from 'vue';
import Vuex from 'vuex';
import { allStores } from 'modules';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
    singleValue: state => state.counter,
    doubleValue: state => state.counter * 2
  },
  mutations: {
    increament: (state) => {
      state.counter += 1;
    },
    decreament: (state) => {
      state.counter -= 1;
    }
  },
  actions: {
    increament: ({ commit }) => {
      commit('increament');
    },
    decreament: ({ commit }) => {
      commit('decreament');
    }
  },
  modules: {
    ...allStores
  }
});
