import Vue from 'vue';
import Router from 'vue-router';
import { allRoutes } from 'modules';

Vue.use(Router);

export default new Router({
  routes: [
    ...allRoutes
  ],
  mode: 'history'
});
