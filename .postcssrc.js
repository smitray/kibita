module.exports = {
  plugins: {
    'postcss-partial-import': {},
    'postcss-crip': {},
    'postcss-nested-props': {},
    'postcss-map': {
      basePath: 'styleVars/',
      maps: [
        'fonts.yml',
        'colors.yml'
      ]
    },
    'postcss-mixins': {},
    'postcss-advanced-variables': {},
    'postcss-short': {},
    'postcss-cssnext': {
      browsers: ["last 5 versions", "Opera 12.1", "safari >= 8", "ie >= 10", "ff >= 20", "ios 6", "android 4", "ie >= 9"]
    },
    'postcss-ref': {},
    'postcss-property-lookup': {},
    'postcss-utilities': {},
    'rucksack-css': {},
    'postcss-extend': {},
    'postcss-merge-rules': {},
    'css-mqpacker': {}
  }
}
