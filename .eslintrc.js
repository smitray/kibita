var path = require('path');

module.exports = {
  'root': true,
  'parser': 'babel-eslint',
  'parserOptions': {
    sourceType: 'module'
  },
  'env': {
    'node': true,
    'browser': true
  },
  'extends': 'airbnb-base',
  'plugins': [
    'html'
  ],
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': path.join(__dirname, 'scripts/eslint.resolver.js')
      }
    }
  },
  'rules': {
    'comma-dangle': [
      'error', {
        'functions': 'ignore'
      }
    ],
    'import/extensions': [
      'error',
      'always', {
        'js': 'never',
        'vue': 'never'
      }
    ],
    'import/no-extraneous-dependencies': [
      'error', {
        'devDependencies': true,
        'optionalDependencies': true
      }
    ],
    'no-param-reassign': [
      'error', {
        'props': false
      }
    ],
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
};
