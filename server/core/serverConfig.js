import convert from 'koa-convert';
import cors from 'kcors';
import bodyParser from 'koa-body';
import session from 'koa-generic-session';
import MongoStore from 'koa-generic-session-mongo';
import helmet from 'koa-helmet';
import config from 'config';
import serve from 'koa-static';
import favicon from 'koa-favicon';
import compress from 'koa-compress';
import isDev from 'isdev';
import { join } from 'path';
import historyFallback from 'koa2-history-api-fallback';
import webpackMidwareConfig from './webpackConfig';
import { cModules, cIoModules, cMiddleware } from '../app';
import { catchErr, statusMessage } from './errorConfig';

export default function baseConfig(app) {
  let sessionParams = {};
  app.keys = [config.get('session.secret')];

  if (config.get('session.db.require')) {
    sessionParams = {
      store: new MongoStore({
        db: config.get('db.dbName'),
        collection: config.get('session.db.collection')
      })
    };
  }

  app.use(convert.compose(
    catchErr,
    cors({
      credentials: true,
      origin: true
    }),
    bodyParser({
      multipart: true
    }),
    helmet(),
    session(sessionParams),
    statusMessage,
    historyFallback()
  ));

  if (isDev) {
    webpackMidwareConfig(app);
  } else {
    app.use(favicon(join(config.get('paths.assetsPath'), 'img', config.get('favicon'))));
    app.use(convert.compose(serve(config.get('paths.dist.path'))));
    if (config.get('server.compress')) {
      app.use(compress());
    }
  }

  cModules(app);
  cIoModules(app);
  app.use(cMiddleware());
}
