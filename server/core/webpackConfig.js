import webpack from 'webpack';
import middleware from 'koa-webpack';
import webpackConfig from '../../scripts/webpack.dev.conf';

export default function webpackMidwareConfig(app) {
  const compiler = webpack(webpackConfig);
  // compiler.plugin('compilation', (compilation) => {
  //   compilation.plugin('html-webpack-plugin-after-emit', (data, cb) => {
  //     middleware.hot.publish({
  //       action: 'reload'
  //     });
  //     cb();
  //   });
  // });
  app.use(middleware({
    compiler,
    dev: {
      publicPath: webpackConfig.output.publicPath
    },
    hot: {
      log: () => {}
    }
  }));
}
