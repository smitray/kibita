// Do not touch the auto loader function
import glob from 'glob';
import Router from 'koa-router';

export default function (app) {
  glob(`${__dirname}/*`, { ignore: '**/index.js' }, (err, matches) => {
    if (err) { throw err; }

    matches.forEach((mod) => {
      const router = require(`${mod}/router`); // eslint-disable-line

      const routes = router.default;
      const { baseUrl } = router;
      const instance = new Router({ prefix: baseUrl });

      routes.forEach((config) => {
        const {
          method = '',
          route = '',
          handlers = []
        } = config;

        const lastHandler = handlers.pop();

        instance[method.toLowerCase()](route, ...handlers, async (ctx) => {
          const hddd = await lastHandler(ctx);
          return hddd;
        });

        app
          .use(instance.routes())
          .use(instance.allowedMethods());
      });
    });
  });
}
