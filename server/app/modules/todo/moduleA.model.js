import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const todoSchema = new mongoose.Schema({
  content: {
    type: String,
    required: true,
    unique: true
  },
  done: {
    type: Boolean,
    default: false
  }
});

todoSchema.plugin(uniqueValidator);
todoSchema.plugin(timestamp);

const todoModel = mongoose.model('todoModel', todoSchema);

// Services

const getTodos = () => (
  new Promise((resolve, reject) => {
    todoModel.find().exec().then((result) => {
      resolve(result);
    }).catch((e) => {
      reject(e);
    });
  })
);

const createTodo = (options) => {
  const tdPr = new Promise((resolve, reject) => {
    todoModel.create(options).then((result) => {
      resolve(result);
    }).catch((err) => {
      reject(err);
    });
  });
  return tdPr;
};

export {
  todoModel,
  getTodos,
  createTodo
};
