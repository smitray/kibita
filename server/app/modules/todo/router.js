import {
  todoIndex,
  crTodo
} from './controller';

export const baseUrl = '/users';

export default [
  {
    method: 'GET',
    route: '/',
    handlers: [
      todoIndex
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      crTodo
    ]
  }
];
