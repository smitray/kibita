import { getTodos, createTodo } from './moduleA.model';

let todos;
let newTd;

const todoIndex = async (ctx) => {
  try {
    todos = await getTodos();
  } catch (e) {
    ctx.throw(404, e.message);
  } finally {
    ctx.body = {
      body: todos
    };
  }
};

const crTodo = async (ctx) => {
  try {
    newTd = await createTodo(ctx.request.body);
  } catch (e) {
    ctx.throw(422, e.message);
  } finally {
    ctx.body = {
      body: newTd
    };
  }
};

export {
  todoIndex,
  crTodo
};
